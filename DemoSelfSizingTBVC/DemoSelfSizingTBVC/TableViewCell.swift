//
//  TableViewCell.swift
//  DemoSelfSizingTBVC
//
//  Created by LeAnhDuc on 28/04/2022.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    var isExpanded = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func expandedStatusChanged(value: Bool){
        contentLabel.numberOfLines = value ? 0 : 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
