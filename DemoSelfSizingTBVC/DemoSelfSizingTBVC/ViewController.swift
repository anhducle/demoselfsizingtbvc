//
//  ViewController.swift
//  DemoSelfSizingTBVC
//
//  Created by LeAnhDuc on 28/04/2022.
//

import UIKit

class ViewController: UIViewController {

    var fakeData = [["Announced in 2014, the Swift programming language has quickly become one of the fastest growing languages in history.", false], ["Announced in 2014, the Swift programming language has quickly become one of the fastest growing languages in history. Swift makes it easy to write software that is incredibly fast and safe by design.",false], ["Announced in 2014, the Swift programming language has quickly become one of the fastest growing languages in history. Swift makes it easy to write software that is incredibly fast and safe by design. Our goals for Swift are ambitious: we want to make programming simple things easy, and difficult things possible.",false]]
    
    @IBOutlet weak var demoTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        demoTableview.estimatedRowHeight = 30
        demoTableview.rowHeight = UITableView.automaticDimension
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fakeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = demoTableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TableViewCell else { return UITableViewCell()}
        cell.contentLabel.text = fakeData[indexPath.row][0] as? String
        cell.contentLabel.numberOfLines = (fakeData[indexPath.row][1] as! Bool) ? 0 : 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        fakeData[indexPath.row][1] = !(fakeData[indexPath.row][1] as! Bool)
        tableView.reloadRows(at: [indexPath], with: .automatic)
        return indexPath
    }
    
}

